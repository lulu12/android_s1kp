// //----------------------------
// // Future<bool> setBatasKp(Periode data) async {
// //   final response = await client.post(
// //       Uri.parse("$baseUrl/addBatasKp.php"),
// //       headers: {"content-type": "application/json"},
// //       body: {
// //         periodeToJson(data)
// //       }
// //   );
// //   if (response.statusCode == 200) {
// //     return true;
// //   } else {
// //     return false;
// //   }
// // }
//
// //--------------------------------------
// import 'package:flutter/material.dart';
// import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
// import 'package:intl/intl.dart';
// import 'package:http/http.dart' as http;
// import 'package:sikp_android/apiservices.dart';
// import 'package:sikp_android/model.dart';
//
// class AddSetBatas extends StatefulWidget {
//   @override
//   _AddSetBatasState createState() => _AddSetBatasState();
// }
//
// class _AddSetBatasState extends State<AddSetBatas> {
//   TextEditingController semesterController = new TextEditingController();
//   TextEditingController tahunController = new TextEditingController();
//   TextEditingController mulaiKpController = new TextEditingController();
//   TextEditingController akhirKpController = new TextEditingController();
//   TextEditingController aktifController = new TextEditingController();
//
//   final GlobalKey<FormState> _formState = GlobalKey<FormState>();
//   bool isLoading = false;
//   Periode periode = new Periode();
//   DateTime mulaiKp, akhirKp;
//   int aktif;
//
//   @override
//   void initState(){
//     mulaiKp = DateTime.now();
//     akhirKp = DateTime.now();
//     aktif = 1;
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text("Set Batas KP"),),
//
//       body: Container(
//         padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
//         child: SingleChildScrollView(
//           child: Stack(
//             children: <Widget>[
//               Form(
//                 key: _formState,
//                 child: Column(
//                   children: <Widget>[
//                     SizedBox(height: 15,),
//                     TextFormField(
//                       decoration: InputDecoration(
//                           labelText: "Semester :",
//                           hintText: "Genap / Ganjil",
//                           border: OutlineInputBorder(),
//                           contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
//                       ),
//                       onSaved: (String value){
//                         this.periode.semester = value;
//                       },
//                     ),
//
//                     SizedBox(height: 15,),
//
//                     TextFormField(
//                       decoration: InputDecoration(
//                           labelText: "Tahun :",
//                           hintText: "Contoh : 2021",
//                           border: OutlineInputBorder(),
//                           contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
//                       ),
//                       keyboardType: TextInputType.number,
//                       onSaved: (String value){
//                         this.periode.tahun = value;
//                       },
//                     ),
//
//                     SizedBox(height: 15,),
//
//                     TextFormField(
//                       decoration: InputDecoration(
//                           labelText: "Mulai KP : ",
//                           hintText: "Tanggal-Bulan-Tahun",
//                           border: OutlineInputBorder(),
//                           contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
//                       ),
//                       onSaved: (String value){
//                         this.periode.mulaiKp = value;
//                       },
//                     ),
//
//                     SizedBox(height: 15,),
//
//                     TextFormField(
//                       decoration: InputDecoration(
//                           labelText: "Akhir KP : ",
//                           hintText: "Tanggal-Bulan-Tahun",
//                           border: OutlineInputBorder(),
//                           contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
//                       ),
//                       onSaved: (String value){
//                         this.periode.akhirKp = value;
//                       },
//                     ),
//
//                     SizedBox(height: 15,),
//
//                     TextFormField(
//                       decoration: InputDecoration(
//                           labelText: "Status : ",
//                           hintText: "Aktif",
//                           border: OutlineInputBorder(),
//                           contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
//                       ),
//                       onSaved: (String value){
//                         this.periode.aktif = value;
//                       },
//                     ),
//
//                     SizedBox(height: 15,),
//
//                     //
//                     // SizedBox(height: 15,),
//                     //
//                     Container(
//                       padding: const EdgeInsets.only(
//                         top: 16,
//                         bottom: 16,
//                       ),
//                       decoration: BoxDecoration(
//                         border: Border(
//                           bottom: BorderSide(
//                             color: Colors.grey,
//                             width: 1.0,
//                           )
//                         )
//                       ),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           Text(
//                             "Mulai Kp",
//                             textAlign: TextAlign.left,
//                             style: const TextStyle(
//                               fontSize: 16,
//                             ),
//                           ),
//                           InkWell(
//                             child: Container(
//                               child: Row(
//                                 children: <Widget>[
//                                   Text(
//                                     DateFormat("dd MMM yyyy").format(mulaiKp),
//                                     textAlign: TextAlign.left,
//                                     style: const TextStyle(fontSize: 16, color: Colors.grey),
//                                   ),
//                                   //Image.asset('assets/image/panah.png')
//                                 ],
//                               ),
//                             ),
//                             onTap: () async{
//                               DateTime newDateTime = await showRoundedDatePicker(
//                                   context: context,
//                                   initialDate: DateTime.now(),
//                                   lastDate: DateTime(DateTime.now().year+35),
//                                   borderRadius: 16,
//                               );
//                               if (newDateTime != null){
//                                 setState(() {
//                                   mulaiKp = newDateTime;
//                                   this.periode.mulaiKp = mulaiKp as String;
//                                 });
//                               }
//                             },
//                           ),
//                         ],
//                       ),
//                     ),
//                     //
//                     // SizedBox(height: 15,),
//                     //
//                     // Container(
//                     //   padding: const EdgeInsets.only(
//                     //     top: 16,
//                     //     bottom: 16,
//                     //   ),
//                     //   decoration: BoxDecoration(
//                     //       border: Border(
//                     //           bottom: BorderSide(
//                     //             color: Colors.grey,
//                     //             width: 1.0,
//                     //           )
//                     //       )
//                     //   ),
//                     //   child: Row(
//                     //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     //     children: <Widget>[
//                     //       Text(
//                     //         "Akhir Kp",
//                     //         textAlign: TextAlign.left,
//                     //         style: const TextStyle(
//                     //           fontSize: 16,
//                     //         ),
//                     //       ),
//                     //       InkWell(
//                     //         child: Container(
//                     //           child: Row(
//                     //             children: <Widget>[
//                     //               Text(
//                     //                 DateFormat("dd MMM yyyy").format(akhirKp),
//                     //                 textAlign: TextAlign.left,
//                     //                 style: const TextStyle(fontSize: 16, color: Colors.grey),
//                     //               ),
//                     //               //Image.asset('assets/image/panah.png')
//                     //             ],
//                     //           ),
//                     //         ),
//                     //         onTap: () async{
//                     //           DateTime newDateTime = await showRoundedDatePicker(
//                     //             context: context,
//                     //             initialDate: DateTime.now(),
//                     //             lastDate: DateTime(DateTime.now().year+35),
//                     //             borderRadius: 16,
//                     //           );
//                     //           if (newDateTime != null){
//                     //             setState(() {
//                     //               akhirKp = newDateTime;
//                     //               this.periode.akhirKp = akhirKp as String;
//                     //             });
//                     //           }
//                     //         },
//                     //       ),
//                     //     ],
//                     //   ),
//                     // ),
//                     //
//                     // SizedBox(height: 15,),
//                     //
//                     // ButtonBar(
//                     //   alignment: MainAxisAlignment.center,
//                     //   children: <Widget>[
//                     //     Text("Aktif"),
//                     //     Radio(
//                     //       value: 1,
//                     //       groupValue: aktif,
//                     //       onChanged: (value){
//                     //         setState(() {
//                     //           aktif = value;
//                     //           this.periode.aktif = aktif as String;
//                     //         });
//                     //       },
//                     //     ),
//                     //   ],
//                     // ),
//                     //
//                     // SizedBox(height: 15,),
//
//
//                     MaterialButton(
//                       minWidth: MediaQuery.of(context).size.width,
//                       padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
//                       color: Colors.cyan,
//                       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
//                       onPressed: (){
//                         return showDialog(
//                             context: context,
//                             builder: (context){
//                               return AlertDialog(
//                                 title: Text("Set Batas KP"),
//                                 content: Text("Aktifkan Batas KP ?"),
//                                 actions: <Widget>[
//                                   TextButton(
//                                     onPressed: () {
//                                       _formState.currentState.save();
//                                       setState(() {
//                                         isLoading = true;
//                                       });
//                                       ApiServices().setBatasKp(this.periode).then((isSucces){
//                                         setState(() {
//                                           isLoading = false;
//                                         });
//                                         if(isSucces){
//                                           Navigator.pop(context);
//                                           Navigator.pop(context);
//                                         }else{
//                                           Navigator.pop(context);
//                                           Navigator.pop(context);
//                                         }
//                                       });
//                                     },
//                                     child: Text("Ya"),
//                                   ),
//                                   TextButton(
//                                     onPressed: (){
//                                       Navigator.pop(context);
//                                     },
//                                     child: Text("Tidak"),
//                                   )
//                                 ],
//                               );
//                             }
//                         );
//                       },
//                       child: Text(
//                         "Simpan",
//                         textAlign: TextAlign.center,
//                         style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
//                       ),
//                     ),
//                     SizedBox(height: 20,),
//                   ],
//                 ),
//               ),
//               isLoading? Stack(
//                 children: <Widget>[
//                   Center(
//                     child: CircularProgressIndicator(),
//                   )
//                 ],
//               )
//                   :Container(),
//               SizedBox(height: 20,)
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
