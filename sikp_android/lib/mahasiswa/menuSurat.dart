
import 'package:flutter/material.dart';
import 'package:sikp_android/apiservices.dart';
import 'package:sikp_android/mahasiswa/addSurat.dart';
import 'package:sikp_android/model.dart';

class MenuSurat extends StatefulWidget {
  @override
  _MenuSuratState createState() => _MenuSuratState();
}

class _MenuSuratState extends State<MenuSurat> {
  List<SK> listSK;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Surat KP"),
        backgroundColor: Colors.blue[700],
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>SuratKeterangan()));
              })
        ],
      ),

      body: FutureBuilder(
          future: ApiServices().getSK(),
          builder: (BuildContext context,
              AsyncSnapshot<List<SK>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listSK = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return Card(
                    margin: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 5.0),
                    child: Container(
                      child: ListTile(
                        title: Text(listSK[position].nim + " - " +
                            listSK[position].semester + " , " + listSK[position].tahun),
                        subtitle: Text(
                            "Lembaga : " + listSK[position].lembaga + "\n" +
                            "Pimpinan : " + listSK[position].pimpinan + "\n" +
                            "Status : " + listSK[position].statusSurat
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listSK.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
