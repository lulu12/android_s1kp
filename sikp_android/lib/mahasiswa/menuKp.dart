
import 'package:flutter/material.dart';
import 'package:sikp_android/apiservices.dart';
import 'package:sikp_android/mahasiswa/addKp.dart';
import 'package:sikp_android/model.dart';

class MenuKp extends StatefulWidget {
  const MenuKp({Key key}) : super(key: key);

  @override
  _MenuKpState createState() => _MenuKpState();
}

class _MenuKpState extends State<MenuKp> {
  List<Kp> listKp;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("KP"),
        backgroundColor: Colors.blue[700],
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>TambahKp()));
              })
        ],
      ),

      body: FutureBuilder(
          future: ApiServices().getKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<Kp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listKp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return Card(
                    margin: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 5.0),
                    child: Container(
                      child: ListTile(
                        title: Text(listKp[position].nim + " - " +
                            listKp[position].semester + " , " + listKp[position].tahun),
                        subtitle: Text(
                                "Judul : " + listKp[position].judul + "\n" +
                                "Lembaga : " + listKp[position].lembaga + "\n" +
                                "Pimpinan : " + listKp[position].pimpinan + "\n" +
                                "Status Ujian : " + listKp[position].statusUjianKp + "\n"
                                "Dosen Pembimbing : " + listKp[position].nidn
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listKp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
