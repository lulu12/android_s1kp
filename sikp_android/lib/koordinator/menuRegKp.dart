
import 'package:flutter/material.dart';
import 'package:sikp_android/apiservices.dart';
import 'package:sikp_android/koordinator/statusKp.dart';
import 'package:sikp_android/model.dart';

class MenuRegKp extends StatefulWidget {
  const MenuRegKp({Key key}) : super(key: key);

  @override
  _MenuRegKpState createState() => _MenuRegKpState();
}

class _MenuRegKpState extends State<MenuRegKp> {
  List<Kp> listKp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("KP"),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: ApiServices().getKpKoor(),
          builder: (BuildContext context,
              AsyncSnapshot<List<Kp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listKp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listKp[position].nim + " - " +
                              listKp[position].semester + " , " + listKp[position].tahun),
                          subtitle: Text(
                                  "Judul : " + listKp[position].judul + "\n" +
                                  "Lembaga : " + listKp[position].lembaga + "\n" +
                                  "Pimpinan : " + listKp[position].pimpinan + "\n" +
                                  "Status Ujian : " + listKp[position].statusUjianKp + "\n"
                                  "Dosen Pembimbing : " + listKp[position].nidn
                          ),
                        ),
                      ),
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>StatusKp()));
                    },
                  );
                },
                itemCount: listKp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
