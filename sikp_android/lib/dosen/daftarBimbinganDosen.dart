
import 'package:flutter/material.dart';
import 'package:sikp_android/apiservices.dart';
import 'package:sikp_android/dosen/ajuinUjianDosen.dart';
import 'package:sikp_android/model.dart';

class DaftarBimbinganDosen extends StatefulWidget {
  const DaftarBimbinganDosen({Key key}) : super(key: key);

  @override
  _DaftarBimbinganDosenState createState() => _DaftarBimbinganDosenState();
}

class _DaftarBimbinganDosenState extends State<DaftarBimbinganDosen> {
  List<Kp> listKp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("KP"),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: ApiServices().getBimbinganDosen(),
          builder: (BuildContext context,
              AsyncSnapshot<List<Kp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listKp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listKp[position].nim + " - " +
                              listKp[position].semester + " , " + listKp[position].tahun),
                          subtitle: Text(
                              "Judul : " + listKp[position].judul + "\n" +
                                  "Status Ujian : " + listKp[position].pengajuanUjian
                          ),
                        ),
                      ),
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>AjuinUjianDosen()));
                    },
                  );
                },
                itemCount: listKp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
