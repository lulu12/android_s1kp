import 'package:flutter/material.dart';
import 'package:sikp_android/apiservices.dart';
import 'package:sikp_android/homekoor.dart';
import 'package:sikp_android/koordinator/addSetBatas.dart';
import 'package:sikp_android/model.dart';

class AturBatasKP extends StatefulWidget {
  @override
  _AturBatasKPState createState() => _AturBatasKPState();
}

class _AturBatasKPState extends State<AturBatasKP> {
  List<Periode> listPeriode;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Batas Kerja Praktek"),
        backgroundColor: Colors.blue[700],
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>AddSetBatas()));
              })
        ],
      ),

      body: FutureBuilder(
          future: ApiServices().getPeriode(),
          builder: (BuildContext context,
              AsyncSnapshot<List<Periode>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listPeriode = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return Card(
                    margin: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 5.0),
                    child: Container(
                      child: ListTile(
                        title: Text(listPeriode[position].semester + " - " +
                            listPeriode[position].tahun),
                        subtitle: Text(
                            "Mulai KP : " + listPeriode[position].mulaiKp + "\n" +
                            "Akhir KP : " + listPeriode[position].akhirKp + "\n" +
                            "Status : " + listPeriode[position].aktif),
                      ),
                    ),
                  );
                },
                itemCount: listPeriode.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}