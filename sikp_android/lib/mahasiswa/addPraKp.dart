import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path/path.dart' as path;

class AjuinPraKp extends StatefulWidget {
  @override
  _AjuinPraKpState createState() => _AjuinPraKpState();
}

class _AjuinPraKpState extends State<AjuinPraKp> {
  TextEditingController judulController = new TextEditingController();
  TextEditingController toolsController = new TextEditingController();
  TextEditingController spekController  = new TextEditingController();
  TextEditingController lembagaController = new TextEditingController();
  TextEditingController pimpinanController = new TextEditingController();
  TextEditingController noTelpController = new TextEditingController();
  TextEditingController alamatController = new TextEditingController();
  TextEditingController faxController = new TextEditingController();
  TextEditingController nimController = new TextEditingController(text: "72180233");
  TextEditingController semesterController = new TextEditingController(text: "Ganjil");
  TextEditingController tahunController = new TextEditingController(text: "2021");

  File _file;
  void getFile()async{
    File file= await FilePicker.getFile(type: FileType.custom, allowedExtensions: ["pdf"]);
    setState(() {
      _file = file;
    });
  }

  save(File file, String fileName) async{
    var request = http.MultipartRequest('POST', Uri.parse("https://10.0.2.2/sikp_db/addPraKp.php"));
    request.files.add(
        http.MultipartFile(
            "dokumenPraKp",
            file.readAsBytes().asStream(),
            file.lengthSync(),
            filename: fileName.split("/").last
        )
    );
    request.fields.addAll(
        {
          "judul": judulController.text,
          "tools": toolsController.text,
          "spesifikasi": spekController.text,
          "lembaga": lembagaController.text,
          "pimpinan": pimpinanController.text,
          "noTelp": noTelpController.text,
          "alamat": alamatController.text,
          "fax": faxController.text,
          "nim": nimController.text,
          "semester": semesterController.text,
          "tahun": tahunController.text,
        }
    );
    var response = await request.send();
  }

//----------------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Pengajuan Pra KP"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),
                  TextFormField(
                    controller: nimController,
                    decoration: InputDecoration(
                        labelText: "NIM :",
                        hintText: "contoh : 72180233",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                    enabled: false,
                  ),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: semesterController,
                    decoration: InputDecoration(
                        labelText: "Semester :",
                        hintText: "Genap / Ganjil",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.text,
                    enabled: false,
                  ),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: tahunController,
                    decoration: InputDecoration(
                        labelText: "Tahun :",
                        hintText: "contoh : 2021",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                    enabled: false,
                  ),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: judulController,
                    decoration: InputDecoration(
                        labelText: "Judul :",
                        hintText: "contoh : Pembuatan Aplikasi E-Warta",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.text,
                  ),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: toolsController,
                    decoration: InputDecoration(
                        labelText: "Tools :",
                        hintText: "Tools yang digunakan",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.text,
                  ),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: spekController,
                    decoration: InputDecoration(
                        labelText: "Spesifikasi :",
                        hintText: "Spesifikasi yang digunakan",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.text,
                  ),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: lembagaController,
                    decoration: InputDecoration(
                        labelText: "Lembaga :",
                        hintText: "contoh : Shopee",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.text,
                  ),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: pimpinanController,
                    decoration: InputDecoration(
                        labelText: "Nama Pimpinan :",
                        hintText: "contoh : Louis Haga Alnoveus Halawa",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.text,
                  ),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: noTelpController,
                    decoration: InputDecoration(
                        labelText: "Nomor Telepon :",
                        hintText: "contoh : 0812xxxxxxxx",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: alamatController,
                    decoration: InputDecoration(
                        labelText: "Alamat :",
                        hintText: "contoh : JL. Menteng",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),


                  SizedBox(height: 15,),
                  TextFormField(
                    controller: faxController,
                    decoration: InputDecoration(
                        labelText: "FAX :",
                        hintText: "contoh : 022xxxx",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),

                  SizedBox(height: 15,),
                  Text("Silahkan Pilih File"),
                  SizedBox(height: 15,),
                  ElevatedButton(
                    child: Text("Harus Berekstensi .pdf"),
                    onPressed: (){
                      getFile();
                    },

                  ),
                  SizedBox(height: 15,),
                  MaterialButton(
                    minWidth: MediaQuery
                        .of(context)
                        .size
                        .width,
                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    color: Colors.cyan,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    onPressed: () {
                      save(_file, _file.path);
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Simpan",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
