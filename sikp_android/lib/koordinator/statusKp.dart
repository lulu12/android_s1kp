import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:http/http.dart' show Client;

class StatusKp extends StatefulWidget {
  const StatusKp({Key key}) : super(key: key);

  @override
  _StatusKpState createState() => _StatusKpState();
}

class _StatusKpState extends State<StatusKp> {
  String dosen;
  List listDosen = ["11111111", "12222222"];

  TextEditingController nimController = new TextEditingController(text: "72180233");


  saveDosen()async{
    final response = await http.post(Uri.parse("https://10.0.2.2/sikp_db/ubahDosbim.php"),
        body: {
          "nim": nimController.text,
          "nidn": dosen
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Pengajuan KP"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),
                  Text("Silahkan Pilih Dosen Pembimbing dari NIM berikut"),
                  SizedBox(height: 15,),
                  TextFormField(
                    controller: nimController,
                    decoration: InputDecoration(
                        labelText: "NIM : ",
                        hintText: "",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),
                  DropdownButtonFormField(
                    hint: Text("Pilih Dosen : "),
                    isExpanded: true,
                    value: dosen,
                    onChanged: (newValue){
                      setState(() {
                        dosen = newValue;
                      });
                    },
                    items: listDosen.map((valueItem){
                      return DropdownMenuItem(
                        value: valueItem,
                        child: Text(valueItem),
                      );
                    }).toList(),
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        enabledBorder: new OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.cyan, width: 2),
                            borderRadius: BorderRadius.circular(10)
                        )
                    ),
                  ),
                  SizedBox(height: 15,),
                  MaterialButton(
                    minWidth: MediaQuery
                        .of(context)
                        .size
                        .width,
                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    color: Colors.cyan,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    onPressed: () {
                      saveDosen();
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Simpan",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),

                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
