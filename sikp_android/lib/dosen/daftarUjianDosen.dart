
import 'package:flutter/material.dart';
import 'package:sikp_android/apiservices.dart';
import 'package:sikp_android/model.dart';

class DaftarUjianDosen extends StatefulWidget {
  const DaftarUjianDosen({Key key}) : super(key: key);

  @override
  _DaftarUjianDosenState createState() => _DaftarUjianDosenState();
}

class _DaftarUjianDosenState extends State<DaftarUjianDosen> {
  List<Ujian> listUjian;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Daftar Ujian"),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: ApiServices().getUjianDosen(),
          builder: (BuildContext context,
              AsyncSnapshot<List<Ujian>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listUjian = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listUjian[position].nim),
                          subtitle: Text(
                                  "Tanggal Ujian : " + listUjian[position].tglUjian + "\n" +
                                  "JamUjian : " + listUjian[position].jamUjian + "\n" +
                                  "Ruangan : " + listUjian[position].namaRuang

                          ),
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listUjian.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
