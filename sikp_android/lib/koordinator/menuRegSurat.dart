
import 'package:flutter/material.dart';
import 'package:sikp_android/apiservices.dart';
import 'package:sikp_android/koordinator/statusSurat.dart';
import 'package:sikp_android/model.dart';

class MenuRegSurat extends StatefulWidget {
  const MenuRegSurat({Key key}) : super(key: key);

  @override
  _MenuRegSuratState createState() => _MenuRegSuratState();
}

class _MenuRegSuratState extends State<MenuRegSurat> {
  List<SK> listSK;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Surat KP"),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: ApiServices().getSKKoor(),
          builder: (BuildContext context,
              AsyncSnapshot<List<SK>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listSK = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listSK[position].nim + " - " +
                              listSK[position].semester + " , " + listSK[position].tahun),
                          subtitle: Text(
                              "Lembaga : " + listSK[position].lembaga + "\n" +
                                  "Pimpinan : " + listSK[position].pimpinan + "\n" +
                                  "Status : " + listSK[position].statusSurat
                          ),
                        ),
                      ),
                    ),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context)=>StatusSurat()),
                      );
                    },
                  );
                },
                itemCount: listSK.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
