
import 'package:flutter/material.dart';
import 'package:sikp_android/apiservices.dart';
import 'package:sikp_android/koordinator/ajuinUjian.dart';
import 'package:sikp_android/model.dart';

class DaftarBimbinganKoor extends StatefulWidget {
  const DaftarBimbinganKoor({Key key}) : super(key: key);

  @override
  _DaftarBimbinganKoorState createState() => _DaftarBimbinganKoorState();
}

class _DaftarBimbinganKoorState extends State<DaftarBimbinganKoor> {
  List<Kp> listKp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("KP"),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: ApiServices().getBimbinganKoor(),
          builder: (BuildContext context,
              AsyncSnapshot<List<Kp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listKp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listKp[position].nim + " - " +
                              listKp[position].semester + " , " + listKp[position].tahun),
                          subtitle: Text(
                                  "Judul : " + listKp[position].judul + "\n" +
                                  "Status Ujian : " + listKp[position].statusUjianKp
                          ),
                        ),
                      ),
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>AjuinUjian()));
                    },
                  );
                },
                itemCount: listKp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
