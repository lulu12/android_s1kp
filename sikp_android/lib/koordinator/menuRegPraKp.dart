
import 'package:flutter/material.dart';
import 'package:sikp_android/apiservices.dart';
import 'package:sikp_android/koordinator/statusPraKp.dart';
import 'package:sikp_android/model.dart';

class MenuRegPraKp extends StatefulWidget {
  const MenuRegPraKp({Key key}) : super(key: key);

  @override
  _MenuRegPraKpState createState() => _MenuRegPraKpState();
}

class _MenuRegPraKpState extends State<MenuRegPraKp> {
  List<PraKp> listPraKp;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pra KP"),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: ApiServices().getPraKpKoor(),
          builder: (BuildContext context,
              AsyncSnapshot<List<PraKp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listPraKp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listPraKp[position].nim + " - " +
                              listPraKp[position].semester + " , " + listPraKp[position].tahun),
                          subtitle: Text(
                              "Judul : " + listPraKp[position].judul + "\n" +
                                  "Lembaga : " + listPraKp[position].lembaga + "\n" +
                                  "Pimpinan : " + listPraKp[position].pimpinan + "\n" +
                                  "Status : " + listPraKp[position].statusPraKp
                          ),
                        ),
                      ),
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>StatusPraKp()));
                    },
                  );
                },
                itemCount: listPraKp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),
    );
  }
}
