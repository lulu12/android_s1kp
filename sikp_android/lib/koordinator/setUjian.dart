import 'dart:convert';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class SetUjian extends StatefulWidget {
  const SetUjian({Key key}) : super(key: key);

  @override
  _SetUjianState createState() => _SetUjianState();
}

class _SetUjianState extends State<SetUjian> {

  String _setTime;

  String dosenPenguji;
  List listDosenPenguji = ["11111111", "12222222"];

  TextEditingController nim = TextEditingController(text: "72180233");
  TextEditingController _timeController = TextEditingController();
  TextEditingController jamUjian = TextEditingController();

  String ruang;
  List listRuang = ["Didaktos", "Biblos", "Agape", "Hagios", "Chara"];

  DateTime tglUjian;

  TimeOfDay selectedTime = TimeOfDay(hour: 00, minute: 00);
  String _hour, _minute, _time;

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked != null)
      setState(() {
        selectedTime = picked;
        _hour = selectedTime.hour.toString();
        _minute = selectedTime.minute.toString();
        _time = _hour + ' : ' + _minute;
        _timeController.text = _time;
        _timeController.text = formatDate(
            DateTime(2019, 08, 1, selectedTime.hour, selectedTime.minute),
            [hh, ':', nn, " ", am]).toString();
      });
  }

  save() async{
    String strTglUjian = new DateFormat('dd-MM-yyyy').format(tglUjian);
    final response = await http.post(Uri.parse("https://10.0.2.2/sikp_db/setUjian.php"),
        body: {
          "nim": nim.text,
          "nidn": dosenPenguji,
          "tglUjian": strTglUjian,
          "jamUjian": _timeController.text,
          "namaRuang": ruang
        });
  }

  @override
  void initState(){
    tglUjian = DateTime.now();
    //jamUjian = TimeOfDay.now();
    _timeController.text = formatDate(
        DateTime(2019, 08, 1, DateTime.now().hour, DateTime.now().minute),
        [hh, ':', nn, " ", am]).toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Set Ujian"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),
                  TextFormField(
                    controller: nim,
                    decoration: InputDecoration(
                        labelText: "Nim : ",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
                    ),
                    enabled: false,
                  ),
                  SizedBox(height: 15,),

                  DropdownButtonFormField(
                    hint: Text("Pilih Dosen Penguji : "),
                    isExpanded: true,
                    value: dosenPenguji,
                    onChanged: (newValue){
                      setState(() {
                        dosenPenguji = newValue;
                      });
                    },
                    items: listDosenPenguji.map((valueItem){
                      return DropdownMenuItem(
                        value: valueItem,
                        child: Text(valueItem),
                      );
                    }).toList(),
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        enabledBorder: new OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.cyan, width: 2),
                            borderRadius: BorderRadius.circular(10)
                        )
                    ),
                  ),
                  SizedBox(height: 15,),

                  Container(
                    padding: const EdgeInsets.only(
                      top: 16,
                      bottom: 16,
                    ),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                              color: Colors.grey,
                              width: 1.0,
                            )
                        )
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Tanggal Ujian :",
                          textAlign: TextAlign.left,
                          style: const TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        InkWell(
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                Text(
                                  DateFormat("dd MMM yyyy").format(tglUjian),
                                  textAlign: TextAlign.left,
                                  style: const TextStyle(fontSize: 16, color: Colors.grey),
                                ),
                                //Image.asset('assets/image/panah.png')
                              ],
                            ),
                          ),
                          onTap: () async{
                            DateTime newDateTime = await showRoundedDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              lastDate: DateTime(DateTime.now().year+35),
                              borderRadius: 16,
                            );
                            if (newDateTime != null){
                              setState(() {
                                tglUjian = newDateTime;
                              });
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 15,),
                  Column(
                    children: <Widget>[
                      Text(
                        'Choose Time',
                        style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.w600,
                            letterSpacing: 0.5),
                      ),
                      InkWell(
                        onTap: () {
                          _selectTime(context);
                        },
                        child: Container(
                          margin: EdgeInsets.only(top: 30),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(color: Colors.grey[200]),
                          child: TextFormField(
                            style: TextStyle(fontSize: 40),
                            textAlign: TextAlign.center,
                            onSaved: (String val) {
                              _setTime = val;
                            },
                            enabled: false,
                            keyboardType: TextInputType.text,
                            controller: _timeController,
                            decoration: InputDecoration(
                                disabledBorder:
                                UnderlineInputBorder(borderSide: BorderSide.none),
                                // labelText: 'Time',
                                contentPadding: EdgeInsets.all(5)),
                          ),
                        ),
                      ),
                    ],
                  ),

                  // TextFormField(
                  //   controller: jamUjian,
                  //   decoration: InputDecoration(
                  //       labelText: "Jam Ujian :",
                  //       hintText: "Contoh : 14:15 PM",
                  //       border: OutlineInputBorder(),
                  //       contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
                  //   ),
                  // ),
                  SizedBox(height: 15,),

                  DropdownButtonFormField(
                    hint: Text("Pilih Ruangan : "),
                    isExpanded: true,
                    value: ruang,
                    onChanged: (newValue){
                      setState(() {
                        ruang = newValue;
                      });
                    },
                    items: listRuang.map((valueItem){
                      return DropdownMenuItem(
                        value: valueItem,
                        child: Text(valueItem),
                      );
                    }).toList(),
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        enabledBorder: new OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.cyan, width: 2),
                            borderRadius: BorderRadius.circular(10)
                        )
                    ),
                  ),
                  SizedBox(height: 15,),

                  MaterialButton(
                    minWidth: MediaQuery
                        .of(context)
                        .size
                        .width,
                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    color: Colors.cyan,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    onPressed: () {
                      save();
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Simpan",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
