
import 'package:flutter/material.dart';
import 'package:sikp_android/apiservices.dart';
import 'package:sikp_android/mahasiswa/addPraKp.dart';
import 'package:sikp_android/model.dart';


class MenuPraKp extends StatefulWidget {
  @override
  _MenuPraKpState createState() => _MenuPraKpState();
}

class _MenuPraKpState extends State<MenuPraKp> {
  List<PraKp> listPraKp;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pra KP"),
        backgroundColor: Colors.blue[700],
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>AjuinPraKp()));
              })
        ],
      ),

      body: FutureBuilder(
          future: ApiServices().getPraKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<PraKp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listPraKp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return Card(
                    margin: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 5.0),
                    child: Container(
                      child: ListTile(
                        title: Text(listPraKp[position].nim + " - " +
                            listPraKp[position].semester + " , " + listPraKp[position].tahun),
                        subtitle: Text(
                                "Judul : " + listPraKp[position].judul + "\n" +
                                "Lembaga : " + listPraKp[position].lembaga + "\n" +
                                "Pimpinan : " + listPraKp[position].pimpinan + "\n" +
                                "Status : " + listPraKp[position].statusPraKp
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listPraKp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
