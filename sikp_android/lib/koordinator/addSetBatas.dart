import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:sikp_android/apiservices.dart';
import 'package:sikp_android/model.dart';

class AddSetBatas extends StatefulWidget {
  @override
  _AddSetBatasState createState() => _AddSetBatasState();
}

class _AddSetBatasState extends State<AddSetBatas> {
  String semester;
  List listSemester = ["Genap", "Ganjil"];

  DateTime mulaiKp, akhirKp;

  //TextEditingController semesterController = new TextEditingController();
  TextEditingController tahunController = new TextEditingController();
  TextEditingController mulaiKpController = new TextEditingController();
  TextEditingController akhirKpController = new TextEditingController();
  //TextEditingController aktifController = new TextEditingController();

  save() async{
    String strMulaiKp = new DateFormat('dd-MM-yyyy').format(mulaiKp);
    String strAkhirKp = new DateFormat('dd-MM-yyyy').format(akhirKp);
    final response = await http.post(Uri.parse("https://10.0.2.2/sikp_db/addPeriode.php"),
        body: {
        "semester": semester,
        "tahun": tahunController.text,
        "mulaiKp": strMulaiKp,
        "akhirKp": strAkhirKp,
    });
  }

  @override
  void initState(){
    setState(() {
      mulaiKp = DateTime.now();
      akhirKp = DateTime.now();
      super.initState();
    });
  }
//------------------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Set Batas KP"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),

                  DropdownButtonFormField(
                    hint: Text("Pilih Semester : "),
                    isExpanded: true,
                    value: semester,
                    onChanged: (newValue){
                      setState(() {
                        semester = newValue;
                      });
                    },
                    items: listSemester.map((valueItem){
                      return DropdownMenuItem(
                          value: valueItem,
                          child: Text(valueItem),
                      );
                    }).toList(),
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        enabledBorder: new OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.cyan, width: 2),
                            borderRadius: BorderRadius.circular(10)
                        )
                    ),
                  ),

                  // TextFormField(
                  //   controller: semesterController,
                  //   decoration: InputDecoration(
                  //       labelText: "Semester :",
                  //       hintText: "Genap / Ganjil",
                  //       border: OutlineInputBorder(),
                  //       contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
                  //   ),
                  // ),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: tahunController,
                    decoration: InputDecoration(
                        labelText: "Tahun :",
                        hintText: "contoh : 2021",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),

                  SizedBox(height: 15,),
                  // TextFormField(
                  //   controller: mulaiKpController,
                  //   decoration: InputDecoration(
                  //       labelText: "Mulai KP :",
                  //       hintText: "Tanggal-Bulan-Tahun",
                  //       border: OutlineInputBorder(),
                  //       contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
                  //   ),
                  //   keyboardType: TextInputType.datetime,
                  // ),

                  //Mencoba Pakai Tanggal tapi tidak bisaaaaaaa :'(
                  // Container(
                  //   padding: const EdgeInsets.only(
                  //     top: 16,
                  //     bottom: 16,
                  //   ),
                  //   decoration: BoxDecoration(
                  //       border: Border(
                  //           bottom: BorderSide(
                  //             color: Colors.grey,
                  //             width: 1.0,
                  //           )
                  //       )
                  //   ),
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //     children: <Widget>[
                  //       Text(
                  //         "Mulai Kp :",
                  //         textAlign: TextAlign.left,
                  //         style: const TextStyle(
                  //           fontSize: 16,
                  //         ),
                  //       ),
                  //       InkWell(
                  //         child: Container(
                  //           child: Row(
                  //             children: <Widget>[
                  //               Text(
                  //                 DateFormat("dd MMM yyyy").format(mulaiKp),
                  //                 textAlign: TextAlign.left,
                  //                 style: const TextStyle(fontSize: 16, color: Colors.grey),
                  //               ),
                  //               //Image.asset('assets/image/panah.png')
                  //             ],
                  //           ),
                  //         ),
                  //         onTap: () async{
                  //           DateTime newDateTime = await showRoundedDatePicker(
                  //             context: context,
                  //             initialDate: DateTime.now(),
                  //             lastDate: DateTime(DateTime.now().year+35),
                  //             borderRadius: 16,
                  //           );
                  //           if (newDateTime != null){
                  //             setState(() {
                  //               mulaiKp = newDateTime;
                  //             });
                  //           }
                  //         },
                  //       ),
                  //     ],
                  //   ),
                  // ),

                  SizedBox(height: 15,),
                  // TextFormField(
                  //   controller: akhirKpController,
                  //   decoration: InputDecoration(
                  //       labelText: "Akhir KP :",
                  //       hintText: "Tanggal-Bulan-Tahun",
                  //       border: OutlineInputBorder(),
                  //       contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
                  //   ),
                  //   keyboardType: TextInputType.datetime,
                  // ),

                  Container(
                    padding: const EdgeInsets.only(
                      top: 16,
                      bottom: 16,
                    ),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                              color: Colors.grey,
                              width: 1.0,
                            )
                        )
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Akhir Kp :",
                          textAlign: TextAlign.left,
                          style: const TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        InkWell(
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                Text(
                                  DateFormat("dd MMM yyyy").format(akhirKp),
                                  textAlign: TextAlign.left,
                                  style: const TextStyle(fontSize: 16, color: Colors.grey),
                                ),
                                //Image.asset('assets/image/panah.png')
                              ],
                            ),
                          ),
                          onTap: () async{
                            DateTime newDateTime = await showRoundedDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              lastDate: DateTime(DateTime.now().year+35),
                              borderRadius: 16,
                            );
                            if (newDateTime != null){
                              setState(() {
                                akhirKp = newDateTime;
                              });
                            }
                          },
                        ),
                      ],
                    ),
                  ),

                  SizedBox(height: 15,),
                  // TextFormField(
                  //   controller: aktifController,
                  //   decoration: InputDecoration(
                  //       labelText: "Status :",
                  //       hintText: "Aktif",
                  //       border: OutlineInputBorder(),
                  //       contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)
                  //   ),
                  // ),
                  // SizedBox(height: 15,),

                  MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      color: Colors.cyan,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
                      onPressed: (){
                        return showDialog(
                          context: context,
                          builder: (context){
                            return AlertDialog(
                              title: Text("Set Batas KP"),
                              content: Text("Aktifkan Batas KP ?"),
                              actions: <Widget>[
                                TextButton(
                                    onPressed: (){
                                      save();
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                    },
                                    child: Text("Ya")
                                ),
                                TextButton(onPressed: (){Navigator.pop(context);}, child: Text("Tidak"))
                              ],
                            );
                          }
                        );
                  },
                    child: Text(
                      "Simpan",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),

                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
