import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


class Registrasi extends StatefulWidget {
  @override
  _RegistrasiState createState() => _RegistrasiState();
}

class _RegistrasiState extends State<Registrasi> {
  String semester;
  List listSemester = ["Genap", "Ganjil"];

  TextEditingController emailController = new TextEditingController(text: "louis.alnoveus@si.ukdw.ac.id");
  TextEditingController namaController = new TextEditingController();
  TextEditingController nimController = new TextEditingController();
  //TextEditingController semesterController = new TextEditingController();
  TextEditingController tahunController = new TextEditingController();

  saveRegis() async {
    final response = await http.post(
        Uri.parse("https://10.0.2.2/sikp_db/addRegistrasi.php"),
        body: {
          "nim": nimController.text,
          "semester": semester,
          "tahun": tahunController.text,
        }
    );
  }

  saveMhs() async{
    final response = await http.post(
        Uri.parse("https://10.0.2.2/sikp_db/addMahasiswa.php"),
        body: {
          "nim": nimController.text,
          "namaMhs": namaController.text,
          "emailMhs": emailController.text,
        }
    );
  }


//------------------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Registrasi KP"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),
                  TextFormField(
                    controller: nimController,
                    decoration: InputDecoration(
                        labelText: "NIM :",
                        hintText: "contoh : 72180233",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: namaController,
                    decoration: InputDecoration(
                        labelText: "Nama :",
                        hintText: "contoh : Louis Haga Alnoveus Halawa",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: emailController,
                    decoration: InputDecoration(
                        labelText: "Email :",
                        hintText: "....@si.ukdw.ac.id",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    enabled: false,
                  ),

                  SizedBox(height: 15,),
                  // TextFormField(
                  //   controller: semesterController,
                  //   decoration: InputDecoration(
                  //       labelText: "Semester :",
                  //       hintText: "Genap / Ganjil",
                  //       border: OutlineInputBorder(),
                  //       contentPadding: EdgeInsets.fromLTRB(
                  //           20.0, 15.0, 20.0, 15.0)
                  //   ),
                  // ),
                  DropdownButtonFormField(
                    hint: Text("Pilih Semester : "),
                    isExpanded: true,
                    value: semester,
                    onChanged: (newValue){
                      setState(() {
                        semester = newValue;
                      });
                    },
                    items: listSemester.map((valueItem){
                      return DropdownMenuItem(
                        value: valueItem,
                        child: Text(valueItem),
                      );
                    }).toList(),
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        enabledBorder: new OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.cyan, width: 2),
                            borderRadius: BorderRadius.circular(10)
                        )
                    ),
                  ),


                  SizedBox(height: 15,),
                  TextFormField(
                    controller: tahunController,
                    decoration: InputDecoration(
                        labelText: "Tahun :",
                        hintText: "contoh : 2021",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),

                  SizedBox(height: 15,),
                  Text("Jika Sudah Registrasi, Tidak Perlu Mengisi Data"),
                  SizedBox(height: 15,),
                  MaterialButton(
                    minWidth: MediaQuery
                        .of(context)
                        .size
                        .width,
                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    color: Colors.cyan,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    onPressed: () {
                      saveMhs();
                      saveRegis();
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Simpan",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
