import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sikp_android/homedosen.dart';
import 'package:sikp_android/homekoor.dart';

import 'homemahasiswa.dart';

class LoginScreen extends StatefulWidget {
  //GoogleSignInAccount _currentUser;
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  Future<void> _prosesLogin() async{
    final googleUser = await GoogleSignIn().signIn();
    if(googleUser != null && googleUser.email.contains("si.ukdw.ac.id")){
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => HomeMahasiswa(nama: googleUser.displayName, email: googleUser.email, foto: googleUser.photoUrl,))
      );
    } else if(googleUser != null && googleUser.email.contains("gmail.com")){
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => HomeDosen(nama: googleUser.displayName, email: googleUser.email, foto: googleUser.photoUrl,))
      );
    } else if(googleUser != null && googleUser.email.contains("students.ukdw.ac.id")){
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => HomeKoor(nama: googleUser.displayName, email: googleUser.email, foto: googleUser.photoUrl,))
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Image.asset('assets/image/logo.png'),
            SignInButton(Buttons.Google, text: 'Sign In With Google',
                onPressed: () => _prosesLogin()
            ),
            Text("\u00a9 2021 rel")
          ],
        ),
      ),
    );
  }
  }





