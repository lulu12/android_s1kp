import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sikp_android/dosen/daftarBimbinganDosen.dart';
import 'package:sikp_android/dosen/daftarUjianDosen.dart';
import 'package:sikp_android/loginscreen.dart';

class HomeDosen extends StatefulWidget {
  final String nama, email, foto;

  HomeDosen({this.nama, this.email, this.foto});
  @override
  _HomeDosenState createState() => _HomeDosenState();
}

class _HomeDosenState extends State<HomeDosen> {
  //Function
  Future<void> _prosesLogout() async {
    final googleUser = await GoogleSignIn().signOut();
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (context) => LoginScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Page Dosen"),
        backgroundColor: Colors.blue[700],
      ),
      backgroundColor: Colors.blue[100],
      drawer: Drawer(
        child: Container(
          color: Colors.blue[100],
          child: ListView(
            children: <Widget>[
              UserAccountsDrawerHeader(accountName: Text("${widget.nama}"),
                accountEmail: Text("${widget.email}"),
                currentAccountPicture: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Image(
                    image: NetworkImage(
                        widget.foto
                    ),
                  ),
                ),
              ),
              Divider(
                color: Colors.black,
                height: 10,
                indent: 10,
                endIndent: 10,
              ),
              ListTile(
                title: Text("Logout"),
                trailing: Icon(Icons.exit_to_app),
                onTap: () => _prosesLogout(),
              ),
            ],
          ),
        ),
      ),

      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>DaftarBimbinganDosen()));
                },
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.my_library_books, size: 70.0, color: Colors.white),
                      Text("Daftar Bimbingan KP", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>DaftarUjianDosen()));},
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.my_library_books, size: 70.0, color: Colors.white),
                      Text("Daftar Ujian KP", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
