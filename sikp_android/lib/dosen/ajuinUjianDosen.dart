import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class AjuinUjianDosen extends StatefulWidget {
  const AjuinUjianDosen({Key key}) : super(key: key);

  @override
  _AjuinUjianDosenState createState() => _AjuinUjianDosenState();
}

class _AjuinUjianDosenState extends State<AjuinUjianDosen> {
  TextEditingController nimController = new TextEditingController(text: "72180233");

  ajukan() async{
    final response = await http.post(Uri.parse("https://10.0.2.2/sikp_db/ajukanUjian.php"),
        body: {
          "nim": nimController.text
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Pengajuan Pra KP"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),
                  Text("Apakah anda setuju untuk mengajukan ujian dengan nim dibawah ini ? "),
                  SizedBox(height: 15,),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: nimController,
                    decoration: InputDecoration(
                        labelText: "NIM :",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                    enabled: false,
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: <Widget>[
                      ElevatedButton(
                        onPressed: (){
                          ajukan();
                          Navigator.pop(context);
                        },
                        child: Text("Ajukan"),
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
