import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sikp_android/loginscreen.dart';
import 'package:sikp_android/mahasiswa/addKp.dart';
import 'package:sikp_android/mahasiswa/lihatUjian.dart';
import 'package:sikp_android/mahasiswa/menuKp.dart';
import 'package:sikp_android/mahasiswa/menuPraKp.dart';
import 'package:sikp_android/mahasiswa/menuSurat.dart';
import 'package:sikp_android/mahasiswa/registrasi.dart';

class HomeMahasiswa extends StatefulWidget {
  final String nama, email, foto;

  HomeMahasiswa({this.nama, this.email, this.foto});
  @override
  _HomeMahasiswaState createState() => _HomeMahasiswaState();
}

class _HomeMahasiswaState extends State<HomeMahasiswa> {
  SharedPreferences isLogin;
  //Function
  Future<void> _prosesLogout() async {
    final googleUser = await GoogleSignIn().signOut();
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => LoginScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Page Mahasiswa"),
        backgroundColor: Colors.blue[700],
      ),
      backgroundColor: Colors.blue[100],
      drawer: Drawer(
        child: Container(
          color: Colors.blue[100],
          child: ListView(
            children: <Widget>[
              UserAccountsDrawerHeader(accountName: Text("${widget.nama}"),
                  accountEmail: Text("${widget.email}"),
                currentAccountPicture: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Image(
                    image: NetworkImage(
                      widget.foto
                    ),
                  ),
                ),
              ),
              Divider(
                color: Colors.black,
                height: 10,
                indent: 10,
                endIndent: 10,
              ),
              ListTile(
                title: Text("Registrasi"),
                trailing: Icon(Icons.add),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>Registrasi()));
                },
              ),
              ListTile(
                title: Text("Logout"),
                trailing: Icon(Icons.exit_to_app),
                onTap: () => _prosesLogout(),
              ),
            ],
          ),
        ),
      ),


      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuKp()));
                },
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.work_outline, size: 70.0, color: Colors.white),
                      Text("Pengajuan Kerja Praktek", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuPraKp()));
                },
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.work, size: 70.0, color: Colors.white),
                      Text("Pengajuan Pra Kerja Praktek", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> MenuSurat()));
                },
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books, size: 70.0, color: Colors.white),
                      Text("Pengajuan Surat Keterangan", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context)=> LihatUjian()));},
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.access_time_sharp, size: 70.0, color: Colors.white),
                      Text("Jadwal Ujian", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
