
//-----------------------------Periode Batas KP-------------------------------
import 'dart:convert';

class Periode {
  String idPeriode;
  String semester;
  String tahun;
  String mulaiKp;
  String akhirKp;
  String aktif;

  Periode(
      {this.idPeriode, this.semester, this.tahun, this.mulaiKp, this.akhirKp, this.aktif});

  factory Periode.fromJson(Map<String, dynamic> map){
    return Periode(
        idPeriode: map["idPeriode"],
        semester: map["semester"],
        tahun: map["tahun"],
        mulaiKp: map["mulaiKp"],
        akhirKp: map["akhirKp"],
        aktif: map["aktif"]);
  }

  Map<String, dynamic> toJson() {
    return {
      "idPeriode": idPeriode,
      "semester": semester,
      "tahun": tahun,
      "mulaiKp": mulaiKp,
      "akhirKp": akhirKp,
      "aktif": aktif
    };
  }

  @override
  String toString() {
    return 'Periode{idPeriode: $idPeriode, semester: $semester, tahun: $tahun, mulaiKp: $mulaiKp, akhirkp: $akhirKp, aktif: $aktif}';
  }
}

List<Periode> periodeFromJson(String jsonData){
  final data = json.decode(jsonData);
  return List<Periode>.from(data.map((item) => Periode.fromJson(item)));
}

String periodeToJson(Periode data){
  final jsonData = data.toJson();
  return json.encode(jsonData);
}

//----------------------------------------------Dosen---------------------------

//----------------------------------------------Mahasiswa-----------------------
class Mahasiswa {
  String nim;
  String namaMhs;
  String emailMhs;

  Mahasiswa({this.nim, this.namaMhs, this.emailMhs});

  factory Mahasiswa.fromJson(Map<String, dynamic> map){
    return Mahasiswa(
        nim: map["nim"],
        namaMhs: map["namaMhs"],
        emailMhs: map["emailMhs"]
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "nim": nim,
      "namaMhs": namaMhs,
      "emailMhs": emailMhs,
    };
  }

  @override
  String toString() {
    return 'Mahasiswa{nim: $nim, namaMhs: $namaMhs, emailMhs: $emailMhs}';
  }
}

List<Mahasiswa> mahasiswaFromJson(String jsonData){
  final data = json.decode(jsonData);
  return List<Mahasiswa>.from(data.map((item) => Mahasiswa.fromJson(item)));
}

String mahasiswaToJson(Periode data){
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
//------------------------------------------------KP----------------------------
class Kp {
  String idKp;
  String judul;
  String tools;
  String spesifikasi;
  String lembaga;
  String pimpinan;
  String noTelp;
  String alamat;
  String fax;
  String dokumenKp;
  String statusUjianKp;
  String nidn;
  String pengajuanUjian;
  String nim;
  String semester;
  String tahun;

  Kp(
      {this.idKp, this.judul, this.tools, this.spesifikasi,  this.lembaga, this.pimpinan, this.noTelp, this.alamat, this.fax, this.dokumenKp, this.statusUjianKp, this.nidn, this.pengajuanUjian, this.nim, this.semester, this.tahun});

  factory Kp.fromJson(Map<String, dynamic> map){
    return Kp(
        idKp: map["idPraKp"],
        judul: map["judul"],
        tools: map["tools"],
        spesifikasi: map["spesifikasi"],
        lembaga: map["lembaga"],
        pimpinan: map["pimpinan"],
        noTelp: map["noTelp"],
        alamat: map["alamat"],
        fax: map["fax"],
        dokumenKp: map["dokumenKp"],
        statusUjianKp: map["statusUjianKp"],
        nidn: map["nidn"],
        pengajuanUjian: map["pengajuanUjian"],
        nim: map["nim"],
        semester: map["semester"],
        tahun: map["tahun"]
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "idKp": idKp,
      "judul": judul,
      "tools": tools,
      "spesifikasi": spesifikasi,
      "lembaga": lembaga,
      "pimpinan": pimpinan,
      "noTelp": noTelp,
      "alamat": alamat,
      "fax": fax,
      "dokumenKp": dokumenKp,
      "statusUjianKp": statusUjianKp,
      "nidn": nidn,
      "pengajuanUjian": pengajuanUjian,
      "nim": nim,
      "semester": semester,
      "tahun": tahun
    };
  }

  @override
  String toString() {
    return 'Kp{idKp: $idKp, judul: $judul, tools: $tools, spesifikasi: $spesifikasi, lembaga: $lembaga, pimpinan: $pimpinan, noTelp: $noTelp, alamat: $alamat, fax: $fax, dokumenKp: $dokumenKp, statusUjianKp: $statusUjianKp, nidn: $nidn, pengajuanUjian: $pengajuanUjian, nim: $nim, semester: $semester, tahun: $tahun}';
  }
}

List<Kp> kpFromJson(String jsonData){
  final data = json.decode(jsonData);
  return List<Kp>.from(data.map((item) => Kp.fromJson(item)));
}

String kpToJson(Kp data){
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
//------------------------------------------------PraKP-------------------------
class PraKp {
  String idPraKp;
  String judul;
  String tools;
  String spesifikasi;
  String lembaga;
  String pimpinan;
  String noTelp;
  String alamat;
  String fax;
  String dokumenPraKp;
  String statusPraKp;
  String nim;
  String semester;
  String tahun;

  PraKp(
      {this.idPraKp, this.judul, this.tools, this.spesifikasi,  this.lembaga, this.pimpinan, this.noTelp, this.alamat, this.fax, this.dokumenPraKp, this.statusPraKp, this.nim, this.semester, this.tahun});

  factory PraKp.fromJson(Map<String, dynamic> map){
    return PraKp(
        idPraKp: map["idPraKp"],
        judul: map["judul"],
        tools: map["tools"],
        spesifikasi: map["spesifikasi"],
        lembaga: map["lembaga"],
        pimpinan: map["pimpinan"],
        noTelp: map["noTelp"],
        alamat: map["alamat"],
        fax: map["fax"],
        dokumenPraKp: map["dokumenPraKp"],
        statusPraKp: map["statusPraKp"],
        nim: map["nim"],
        semester: map["semester"],
        tahun: map["tahun"]
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "idPraKp": idPraKp,
      "judul": judul,
      "tools": tools,
      "spesifikasi": spesifikasi,
      "lembaga": lembaga,
      "pimpinan": pimpinan,
      "noTelp": noTelp,
      "alamat": alamat,
      "fax": fax,
      "dokumenPraKp": dokumenPraKp,
      "statusPraKp": statusPraKp,
      "nim": nim,
      "semester": semester,
      "tahun": tahun
    };
  }

  @override
  String toString() {
    return 'PraKp{idPraKp: $idPraKp, judul: $judul, tools: $tools, spesifikasi: $spesifikasi, lembaga: $lembaga, pimpinan: $pimpinan, noTelp: $noTelp, alamat: $alamat, fax: $fax, dokumenPraKp: $dokumenPraKp, statusPraKp: $statusPraKp, nim: $nim, semester: $semester, tahun: $tahun}';
  }
}

List<PraKp> praKpFromJson(String jsonData){
  final data = json.decode(jsonData);
  return List<PraKp>.from(data.map((item) => PraKp.fromJson(item)));
}

String praKpToJson(PraKp data){
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
//-------------------------------------------------SuratKeterangan--------------
class SK {
  String idSurat;
  String lembaga;
  String pimpinan;
  String noTelp;
  String alamat;
  String fax;
  String dokumenSurat;
  String statusSurat;
  String nim;
  String semester;
  String tahun;

  SK(
      {this.idSurat, this.lembaga, this.pimpinan, this.noTelp, this.alamat, this.fax, this.dokumenSurat, this.statusSurat, this.nim, this.semester, this.tahun});

  factory SK.fromJson(Map<String, dynamic> map){
    return SK(
        idSurat: map["idSurat"],
        lembaga: map["lembaga"],
        pimpinan: map["pimpinan"],
        noTelp: map["noTelp"],
        alamat: map["alamat"],
        fax: map["fax"],
        dokumenSurat: map["dokumenSurat"],
        statusSurat: map["statusSurat"],
        nim: map["nim"],
        semester: map["semester"],
        tahun: map["tahun"]
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "idSurat": idSurat,
      "lembaga": lembaga,
      "pimpinan": pimpinan,
      "noTelp": noTelp,
      "alamat": alamat,
      "fax": fax,
      "dokumenSurat": dokumenSurat,
      "statusSurat": statusSurat,
      "nim": nim,
      "semester": semester,
      "tahun": tahun
    };
  }

  @override
  String toString() {
    return 'SK{idSurat: $idSurat, lembaga: $lembaga, pimpinan: $pimpinan, noTelp: $noTelp, alamat: $alamat, fax: $fax, dokumenSurat: $dokumenSurat, statusSurat: $statusSurat, nim: $nim, semester: $semester, tahun: $tahun}';
  }
}

List<SK> skFromJson(String jsonData){
  final data = json.decode(jsonData);
  return List<SK>.from(data.map((item) => SK.fromJson(item)));
}

String skToJson(SK data){
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
//--------------------------------------------------Ujian-----------------------
class Ujian {
  String idUjian;
  String nim;
  String nidn;
  String tglUjian;
  String jamUjian;
  String namaRuang;

  Ujian({this.idUjian, this.nim, this.nidn, this.tglUjian, this.jamUjian, this.namaRuang});

  factory Ujian.fromJson(Map<String, dynamic> map){
    return Ujian(
        idUjian: map["idUjian"],
        nim: map["nim"],
        nidn: map["nidn"],
        tglUjian: map["tglUjian"],
        jamUjian: map["jamUjian"],
        namaRuang: map["namaRuang"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "idUjian": idUjian,
      "nim": nim,
      "nidn": nidn,
      "tglUjian": tglUjian,
      "jamUjian": jamUjian,
      "namaRuang": namaRuang,
    };
  }

  @override
  String toString() {
    return 'Ujian{idUjian: $idUjian, nim: $nim, nidn: $nidn, tglUjian: $tglUjian, jamUjian: $jamUjian, namaRuang: $namaRuang}';
  }
}

List<Ujian> ujianFromJson(String jsonData){
  final data = json.decode(jsonData);
  return List<Ujian>.from(data.map((item) => Ujian.fromJson(item)));
}

String ujianToJson(Ujian data){
  final jsonData = data.toJson();
  return json.encode(jsonData);
}

//---------------------------------------------------Registrasi----------------
class Registrasi {
  String semester;
  String tahun;

  Registrasi(
      {this.semester, this.tahun});

  factory Registrasi.fromJson(Map<String, dynamic> map){
    return Registrasi(
        semester: map["semester"],
        tahun: map["tahun"]
        );
  }

  Map<String, dynamic> toJson() {
    return {
      "semester": semester,
      "tahun": tahun
    };
  }

  @override
  String toString() {
    return 'Registrasi{semester: $semester, tahun: $tahun}';
  }
}

List<Registrasi> registrasiFromJson(String jsonData){
  final data = json.decode(jsonData);
  return List<Registrasi>.from(data.map((item) => Periode.fromJson(item)));
}

String RegistrasiToJson(Periode data){
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
//---------------------------------------------------Ruang----------------------


