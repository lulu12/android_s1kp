import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' show Client;
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import 'model.dart';


class ApiServices{
  final String baseUrl = "https://10.0.2.2/sikp_db";
  Client client = Client();

  Future<List<Periode>> getPeriode() async{
    final response = await client.get(Uri.parse("$baseUrl/getPeriode.php"));
    if(response.statusCode == 200){
      return periodeFromJson(response.body);
    }else{
      return null;
    }
  }

  Future<List<SK>> getSK() async{
    final response = await client.get(Uri.parse("$baseUrl/getSK.php"));
    if(response.statusCode == 200){
      return skFromJson(response.body);
    }else{
      return null;
    }
  }

  Future<List<SK>> getSKKoor() async{
    final response = await client.get(Uri.parse("$baseUrl/getSkKoor.php"));
    if(response.statusCode == 200){
      return skFromJson(response.body);
    }else{
      return null;
    }
  }

  Future<List<PraKp>> getPraKp() async{
    final response = await client.get(Uri.parse("$baseUrl/getPraKp.php"));
    if(response.statusCode == 200){
      return praKpFromJson(response.body);
    }else{
      return null;
    }
  }

  Future<List<PraKp>> getPraKpKoor() async{
    final response = await client.get(Uri.parse("$baseUrl/getPraKpKoor.php"));
    if(response.statusCode == 200){
      return praKpFromJson(response.body);
    }else{
      return null;
    }
  }

  Future<List<Kp>> getKp() async{
    final response = await client.get(Uri.parse("$baseUrl/getKp.php"));
    if(response.statusCode == 200){
      return kpFromJson(response.body);
    }else{
      return null;
    }
  }

  Future<List<Kp>> getKpKoor() async{
    final response = await client.get(Uri.parse("$baseUrl/getKpKoor.php"));
    if(response.statusCode == 200){
      return kpFromJson(response.body);
    }else{
      return null;
    }
  }
  Future<List<Kp>> getBimbinganKoor() async{
    final response = await client.get(Uri.parse("$baseUrl/daftarBimbinganKoor.php"));
    if(response.statusCode == 200){
      return kpFromJson(response.body);
    }else{
      return null;
    }
  }

  Future<List<Kp>> getBimbinganDosen() async{
    final response = await client.get(Uri.parse("$baseUrl/daftarBimbinganDosen.php"));
    if(response.statusCode == 200){
      return kpFromJson(response.body);
    }else{
      return null;
    }
  }

  Future<List<Ujian>> getUjianKoor() async{
    final response = await client.get(Uri.parse("$baseUrl/getUjianKoor.php"));
    if(response.statusCode == 200){
      return ujianFromJson(response.body);
    }else{
      return null;
    }
  }

  Future<List<Ujian>> getUjianDosen() async{
    final response = await client.get(Uri.parse("$baseUrl/getUjianDosen.php"));
    if(response.statusCode == 200){
      return ujianFromJson(response.body);
    }else{
      return null;
    }
  }
  Future<List<Ujian>> getUjianMhs() async{
    final response = await client.get(Uri.parse("$baseUrl/lihatUjianMhs.php"));
    if(response.statusCode == 200){
      return ujianFromJson(response.body);
    }else{
      return null;
    }
  }
}

