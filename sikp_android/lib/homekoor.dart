import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sikp_android/koordinator/daftarBimbinganKoor.dart';
import 'package:sikp_android/koordinator/daftarUjianKoor.dart';
import 'package:sikp_android/koordinator/menuRegKp.dart';
import 'package:sikp_android/koordinator/menuRegPraKp.dart';
import 'package:sikp_android/koordinator/menuRegSurat.dart';
import 'package:sikp_android/koordinator/menuSetBatas.dart';
import 'package:sikp_android/koordinator/menuUjian.dart';
import 'package:sikp_android/loginscreen.dart';

class HomeKoor extends StatefulWidget {
  final String nama, email, foto;

  HomeKoor({this.nama, this.email, this.foto});
  @override
  _HomeKoorState createState() => _HomeKoorState();
}

class _HomeKoorState extends State<HomeKoor> {
  Future<void> _prosesLogout() async {
    final googleUser = await GoogleSignIn().signOut();
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (context) => LoginScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Page Koordinator"),
        backgroundColor: Colors.blue[700],
      ),
      backgroundColor: Colors.blue[100],
      drawer: Drawer(
        child: Container(
          color: Colors.blue[100],
          child: ListView(
            children: <Widget>[
              UserAccountsDrawerHeader(accountName: Text("${widget.nama}"),
                accountEmail: Text("${widget.email}"),
                currentAccountPicture: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Image(
                    image: NetworkImage(
                        widget.foto
                    ),
                  ),
                ),
              ),
              Divider(
                color: Colors.black,
                height: 10,
                indent: 10,
                endIndent: 10,
              ),
              ListTile(
                title: Text("Logout"),
                trailing: Icon(Icons.exit_to_app),
                onTap: () => _prosesLogout(),
              ),

            ],
          ),
        ),
      ),

      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> AturBatasKP()));
                },
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.timelapse_sharp, size: 70.0, color: Colors.white),
                      Text("Atur Batas Pelaksanaan KP", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuUjian()));},
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.timelapse_sharp, size: 70.0, color: Colors.white),
                      Text("Atur Ujian KP", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuRegPraKp()));
                },
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books, size: 70.0, color: Colors.white),
                      Text("Daftar Registrasi Pra KP", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: (){ Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuRegKp()));},
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books, size: 70.0, color: Colors.white),
                      Text("Daftar Registrasi KP", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: (){
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context)=>MenuRegSurat()),
                  );
                },
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books, size: 70.0, color: Colors.white),
                      Text("Daftar Pengajuan SK", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>DaftarBimbinganKoor()));
                },
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books, size: 70.0, color: Colors.white),
                      Text("Daftar Bimbingan KP", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.blue[200],
              child: InkWell(
                onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context)=>DaftarUjianKoor()));},
                splashColor: Colors.grey,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.library_books, size: 70.0, color: Colors.white),
                      Text("Daftar Ujian KP", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),

    );
  }
}